package com.mapstruct;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.time.LocalTime;

@Mapper(componentModel = "spring")
public interface TypeMapper {
   // TypeMapper INSTANCE = Mappers.getMapper(TypeMapper.class);

   @Mapping(expression = "java(fruit.getName() + vehicle.getModel() )", target = "enginename")
   @Mapping(source = "vehicle.model",target = "modelname")
   @Mapping(source = "vehicle.seatcount", target = "passenger")
    Spaceship toDto(Fruit fruit, Vehicle vehicle);
}
