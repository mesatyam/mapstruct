package com.mapstruct;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class TypeController {

    private final TypeMapper typeMapper;

    @GetMapping("/dto")
    public Spaceship todto(){
        Fruit fruit = new Fruit("Mango",5);
        Vehicle vehicle = new Vehicle("T799",4);
        Spaceship spaceship =  typeMapper.toDto(fruit,vehicle);
        System.out.println(spaceship);
        return spaceship;
    }
}
