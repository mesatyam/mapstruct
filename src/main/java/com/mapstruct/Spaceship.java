package com.mapstruct;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Spaceship {
    private String modelname;
    private String passenger;
    private int qty;
    private String enginename;
}
